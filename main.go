package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"

	datablob "blobfish.com/datablob"
	api "blobfish.com/servblob/api"
	ws "blobfish.com/servblob/ws"
	gameturn "blobfish.com/turnblob"
)

func setupRoutes(hub *ws.Hub) mux.Router {
	router := mux.NewRouter()

	router.HandleFunc("/ws/{id}", func(w http.ResponseWriter, r *http.Request) {
		ws.ServeWs(hub, w, r)
	})
	router.HandleFunc("/create", api.Create).Methods("POST")
	router.HandleFunc("/check", api.Check).Methods("POST")

	return *router
}

func main() {
	fmt.Println("Launch server")
	flag.Parse()
	err := datablob.InitSchema()
	if err != nil {
		panic(err)
	}

	hub := ws.NewHub()
	go hub.Run()

	turn := gameturn.Turn{Hub: hub}
	go turn.Run()

	router := setupRoutes(hub)

	log.Fatal(http.ListenAndServe("localhost:8080", &router))
}
