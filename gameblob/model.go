package gameblob

import (
	"fmt"
)

type Error struct {
	Error   error
	Message string
}

func (e Error) String() string {
	return fmt.Sprintf(e.Message)
}
