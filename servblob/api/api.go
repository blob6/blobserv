package api

import (
	"encoding/json"
	"fmt"
	"net/http"

	datablob "blobfish.com/datablob"
)

func parseCreateBody(body CreateUserBody, license *datablob.License, user *datablob.User) {
	user.Username = body.Username
	user.Wallet = body.Wallet
	license.Name = body.Username
	license.Level = 1
}

func Create(w http.ResponseWriter, r *http.Request) {
	var userBody CreateUserBody
	var bodyResponse string

	var license datablob.License
	var user datablob.User
	var fisher datablob.Fisher
	alreadyUsed := false

	w.Header().Set("Content-Type", "application/json")

	err := json.NewDecoder(r.Body).Decode(&userBody)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}

	parseCreateBody(userBody, &license, &user)

	if (datablob.GetLicenseByUsername(license.Name) == datablob.License{}) {
		license.UserID = user.ID
		license.User = user

		errFisher := datablob.InsertFisher(userBody.Username, license, &fisher)
		if errFisher != nil {
			w.WriteHeader(http.StatusInternalServerError)
		}
	} else {
		alreadyUsed = true
	}

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		bodyResponse = fmt.Sprintf("{\"error\":\"internal error\"}")
	} else if alreadyUsed {
		w.WriteHeader(http.StatusConflict)
		bodyResponse = fmt.Sprintf("{\"error\":\"username already used\"}")
	} else {
		w.WriteHeader(http.StatusOK)
		jsonFisher, err := json.Marshal(fisher)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			bodyResponse = fmt.Sprintf("{\"error\":\"internal error\"}")
		}
		bodyResponse = string(jsonFisher)
	}
	fmt.Fprintf(w, bodyResponse)
}

func Check(w http.ResponseWriter, r *http.Request) {
	var userCheckBody CheckUserBody
	var bodyResponse string

	w.Header().Set("Content-Type", "application/json")

	err := json.NewDecoder(r.Body).Decode(&userCheckBody)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}

	if (datablob.GetUserByWalletAndUsername(userCheckBody.Wallet, userCheckBody.Username) == datablob.User{}) {
		w.WriteHeader(http.StatusConflict)
		bodyResponse = fmt.Sprintf("{\"error\":\"User does not exist\"")
	} else {
		w.WriteHeader(http.StatusOK)
		bodyResponse = fmt.Sprintf("{\"result\":\"%t\"}", true)
	}

	fmt.Fprintf(w, bodyResponse)
}
