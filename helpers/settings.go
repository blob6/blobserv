package helpers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
)

type DatabaseSettings struct {
	Username     string `json:"username"`
	Password     string `json:"password"`
	Databasename string `json:"databasename"`
	Url          string `json:"url"`
	Port         string `json:"port"`
}

type Settings struct {
	Databasesettings DatabaseSettings `json:"database"`
}

func GetDbSettings(settings *Settings) {
	jsonFile, err := os.Open("settings.json")
	if err != nil {
		return
	}

	defer jsonFile.Close()

	byteValue, _ := ioutil.ReadAll(jsonFile)
	json.Unmarshal(byteValue, &settings)
}

func main() {
	var settings Settings
	GetDbSettings(&settings)

	fmt.Println("DB username: " + settings.Databasesettings.Username)
	fmt.Println("DB password: " + settings.Databasesettings.Password)
	fmt.Println("DB databaseName: " + settings.Databasesettings.Databasename)
}
