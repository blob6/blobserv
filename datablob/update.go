package datablob

func updateSquareWaterWithFish(squareWater SquareWater, fishID uint64) error {
	squareWater.FishID = fishID

	db, err := getDB()
	if err != nil {
		return err
	}

	db.Save(&squareWater)
	return nil
}

func addFisherInSquareFront(id uint64, idMap uint64, x int, y int) error {
	db, err := getDB()
	if err != nil {
		return err
	}

	square, errSquare := getSquareByMapAndPos(idMap, x, y)
	if errSquare != nil {
		return errSquare
	}

	db.Model(SquareFront{}).Where("square_id = ?", square.ID).Update("fisher_id", id)
	return nil
}

func RemoveFisherFromCurrentMap(squareFront SquareFront) error {
	db, err := getDB()
	if err != nil {
		return err
	}

	db.Model(SquareFront{}).Where("square_id = ?", squareFront.ID).Update("fisher_id", 0)
	return nil
}
