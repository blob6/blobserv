package datablob

type Stats struct {
	Energy  float64 `json:"energy"`
	Power   float64 `json:"power"`
	Storage float64 `json:"storage"`
}

type Position struct {
	X int `json:"x"`
	Y int `json:"y"`
}

type OwnModel struct {
	ID uint64 `"gorm:"primary_key"`
}

type SpecieByMap struct {
	OwnModel
	SpeciesID uint64 `json:"speciesId"`
	MapTypeID uint64 `json:"maptypeId"`
}

type MapType struct {
	OwnModel
	Name string `json:"name"`
}

type Map struct {
	OwnModel
	Name   string  `json:"name"`
	Type   MapType `json:"type"`
	TypeID uint64  `json:"-"`
}

type Square struct {
	OwnModel
	Position Position `gorm:"embedded"`
	MapID    uint64   `json:"-"`
	Map      Map      `json:"-"`
}

type SquareWater struct {
	OwnModel
	Square   Square `json:"square"`
	SquareID uint64 `json:"-"`
	FishID   uint64 `json:"-"`
}

type SquareFront struct {
	OwnModel
	Square   Square `json:"square"`
	SquareID uint64 `json:"-"`
	FisherID uint64 `json:"-"`
}

type Assert struct {
	Shape   uint64 `json:"shape"`
	Pattern uint64 `json:"pattern"`
}

type FishAsserts struct {
	OwnModel
	BodyShape   uint64 `json:"bodyShape"`
	BodyPattern uint64 `json:"bodyPattern"`

	EyeShape   uint64 `json:"eyeShape"`
	EyePattern uint64 `json:"eyePattern"`

	ArmShape   uint64 `json:"armShape"`
	ArmPattern uint64 `json:"armPattern"`

	DorsalShape   uint64 `json:"dorsalShape"`
	DorsalPattern uint64 `json:"dorsalPattern"`

	VentralShape   uint64 `json:"ventralShape"`
	VentralPattern uint64 `json:"ventralPattern"`

	TailShape   uint64 `json:"tailShape"`
	TailPattern uint64 `json:"tailPattern"`

	Accessorie uint64 `json:"accessorie"`
}

type Factors struct {
	OwnModel
	SpeciesCommon float64     `json:"speciesCommon"`
	SpeciesRare   float64     `json:"speciesRare"`
	SpeciesLegend float64     `json:"speciesLegend"`
	Loot          float64     `json:"loot"`
	FishAsserts   FishAsserts `json:"fishAsserts"`
	FishAssertsID uint64      `json:"-"`
}
type Specie struct {
	OwnModel
	Name      string `json:"name"`
	Prefix    string `json:"prefix"`
	Rarity    string `json:"rarity"`
	MaxWeight uint64 `json:"maxWeight"`
	MaxSize   uint64 `json:"maxSize"`
	MaxSpeed  uint64 `json:"maxSpeed"`
}

type Fish struct {
	OwnModel
	Seed     string `json:"seed"`
	SpecieID uint64 `json:"-"`
	Specie   Specie `json:"specie"`

	Weight uint64 `json:"weight"`
	Size   uint64 `json:"size"`
	Speed  uint64 `json:"speed"`

	FishAsserts   FishAsserts `json:"fishAsserts"`
	FishAssertsID uint64      `json:"-"`
}

type User struct {
	OwnModel
	Username string `json:"username"`
	Wallet   string `json:"wallet"`
}

type License struct {
	OwnModel
	Name   string `json:"name"`
	Level  int    `json:"level"`
	UserID uint64 `json:"-"`
	User   User   `json:"user"`
}

type Fisher struct {
	OwnModel
	Name      string  `json:"name"`
	SkinColor uint64  `json:"skinColor"`
	Hairiness uint64  `json:"hairiness"`
	Stats     Stats   `gorm:"embedded";json:"stats"`
	LicenseID uint64  `json:"-"`
	License   License `json:"license"`
}

type WsSquareWater struct {
	SquareWater SquareWater `json:"square_water"`
}

type WsSquareFront struct {
	SquareFront SquareFront `json:"square_front"`
	Fisher      Fisher      `json:"fisher"`
}

type WsMap struct {
	Map          Map             `json:"map"`
	SquaresFront []WsSquareFront `json:"squares_front"`
	SquaresWater []WsSquareWater `json:"squares_water"`
}

type WsMaps struct {
	WsMap []WsMap `json:"maps"`
}
