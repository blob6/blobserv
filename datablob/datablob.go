package datablob

import (
	"fmt"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"

	helper "blobfish.com/helpers"
)

var DB *gorm.DB

func InitSchema() error {
	db, err := getDB()
	if err != nil {
		return err
	}

	db.Migrator().DropTable(&FishAsserts{}, &Map{}, &Assert{}, &Square{},
		&Factors{}, &SquareWater{}, &SquareFront{}, &Specie{},
		&Fish{}, &User{}, &License{}, &Fisher{},
		&MapType{}, &SpecieByMap{})

	db.AutoMigrate(&FishAsserts{}, &Map{}, &Assert{}, &Square{}, &SquareWater{},
		&Factors{}, &SquareFront{}, &Specie{}, &Fish{},
		&User{}, &License{}, &Fisher{},
		&MapType{}, &SpecieByMap{})

	err_factors := addFactors()
	if err != nil {
		return err_factors
	}

	maps, err_map := createMaps()
	if err_map != nil {
		return err_map
	}

	err_species := addSpecies()
	if err_species != nil {
		return err_species
	}

	err_fishes := populateFishes(maps)
	if err_fishes != nil {
		return err_fishes
	}

	return nil
}

func getDB() (*gorm.DB, error) {
	if DB != nil {
		return DB, nil
	}

	var settings helper.Settings
	helper.GetDbSettings(&settings)

	dsn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8mb4&parseTime=True&loc=Local",
		settings.Databasesettings.Username,
		settings.Databasesettings.Password,
		settings.Databasesettings.Url,
		settings.Databasesettings.Port,
		settings.Databasesettings.Databasename)

	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		return nil, err
	}

	DB = db
	return DB, err
}
