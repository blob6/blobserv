package datablob

import (
	"math/rand"
)

func getSquareWatersByMapId(id uint64) ([]SquareWater, error) {
	squareWaters := []SquareWater{}

	db, err := getDB()
	if err != nil {
		return []SquareWater{}, err
	}

	db.Table("square_waters").
		Select("square_waters.ID, square_id, fish_id").
		Joins("left join squares on squares.ID = square_waters.square_id").
		Where("squares.map_id = ?", id).
		Scan(&squareWaters)

	return squareWaters, nil
}

func getRandomSpecie() (Specie, error) {
	species := []Specie{}

	db, err := getDB()
	if err != nil {
		return Specie{}, err
	}

	db.Find(&species)

	return species[rand.Intn(len(species))], nil
}

func getSquareByMapAndPos(idMap uint64, x int, y int) (Square, error) {
	square := Square{}

	db, err := getDB()
	if err != nil {
		return Square{}, err
	}

	db.Where("map_id = ? and x = ? and y = ?", idMap, x, y).Find(&square)
	return square, nil
}

func GetLicenseByUsername(name string) License {
	license := License{}

	db, err := getDB()
	if err != nil {
		return License{}
	}

	db.Where("Name = ?", name).Find(&license)
	return license
}

func GetFishersByUserId(userId uint64) []Fisher {
	var fishers []Fisher

	db, err := getDB()
	if err != nil {
		return []Fisher{}
	}

	db.Table("fishers").
		Select("fishers.id").
		Joins("left join licenses on licenses.id = fishers.license_id").
		Where("licenses.user_id = ?", userId).
		Scan(&fishers)

	return fishers
}

func getSquareFrontByMapId(idMap uint64) []SquareFront {
	var squaresFront []SquareFront

	db, err := getDB()
	if err != nil {
		return []SquareFront{}
	}

	db.Preload("Square").
		Where("squares.map_id = ?", idMap).
		Joins("left join squares on squares.ID = square_fronts.square_id").
		Find(&squaresFront)

	return squaresFront
}

func GetMaps() []Map {
	var maps []Map

	db, err := getDB()
	if err != nil {
		return []Map{}
	}

	db.Find(&maps)
	return maps
}

func getSquareFrontAndWaterByMapId(mapId uint64) ([]SquareFront, []SquareWater) {
	squaresFront := []SquareFront{}
	squaresWater := []SquareWater{}

	db, err := getDB()
	if err != nil {
		return []SquareFront{}, []SquareWater{}
	}

	db.Preload("Square").
		Where("squares.map_id = ?", mapId).
		Joins("left join squares on squares.ID = square_fronts.square_id").
		Find(&squaresFront)

	db.Preload("Square").
		Where("squares.map_id = ?", mapId).
		Joins("left join squares on squares.ID = square_waters.square_id").
		Find(&squaresWater)

	return squaresFront, squaresWater
}

func getFishById(fishId uint64) Fish {
	fish := Fish{}

	db, err := getDB()
	if err != nil {
		return Fish{}
	}

	db.Where("id = ?", fishId).Find(&fish)
	return fish
}

func getFisherById(fisherId uint64) Fisher {
	fisher := Fisher{}

	db, err := getDB()
	if err != nil {
		return Fisher{}
	}

	db.Where("id = ?", fisherId).Find(&fisher)
	return fisher
}

func GetMapsAllDatas() WsMaps {
	var wsMaps WsMaps
	maps := GetMaps()
	for _, aMap := range maps {
		wsMap := WsMap{Map: aMap}
		squaresFront, squaresWater := getSquareFrontAndWaterByMapId(aMap.ID)

		for _, squareFront := range squaresFront {
			var wsSquareFront WsSquareFront
			wsSquareFront.SquareFront = squareFront
			if squareFront.FisherID != 0 {
				wsSquareFront.Fisher = getFisherById(squareFront.FisherID)
			}
			wsMap.SquaresFront = append(wsMap.SquaresFront, wsSquareFront)
		}

		for _, squareWater := range squaresWater {
			var wsSquareWater WsSquareWater
			wsSquareWater.SquareWater = squareWater
			wsMap.SquaresWater = append(wsMap.SquaresWater, wsSquareWater)
		}

		wsMaps.WsMap = append(wsMaps.WsMap, wsMap)
	}

	return wsMaps
}

func GetUserByWalletAndUsername(wallet string, username string) User {
	user := User{}

	db, err := getDB()
	if err != nil {
		return User{}
	}

	db.Where("Wallet = ? AND Username = ?", wallet, username).Find(&user)
	return user
}

func getMapTypeByName(mapTypeName string) MapType {
	mapType := MapType{}

	db, err := getDB()
	if err != nil {
		return MapType{}
	}

	db.Where("name = ?", mapTypeName).Find(&mapType)

	return mapType
}

func getSpeciesByMapTypeId(mapTypeId uint64) []Specie {
	species := []Specie{}

	db, err := getDB()
	if err != nil {
		return []Specie{}
	}

	db.Where("specie_by_maps.map_type_id = ?", mapTypeId).
		Joins("left join specie_by_maps on specie_by_maps.species_id = species.id").
		Find(&species)

	return species
}

func getFactors() Factors {
	factors := Factors{}

	db, err := getDB()
	if err != nil {
		return Factors{}
	}

	db.Preload("FishAsserts").Find(&factors)
	return factors
}
