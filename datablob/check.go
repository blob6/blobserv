package datablob

func IsUserGotThisFisher(id uint64, idFisher uint64) bool {
	fishers := GetFishersByUserId(id)

	for _, fisher := range fishers {
		if fisher.ID == idFisher {
			return true
		}
	}

	return false
}

func IsSquareMapIsAvailable(idMap uint64, x int, y int) bool {
	squaresFront := getSquareFrontByMapId(idMap)

	for _, squareFront := range squaresFront {
		if (squareFront.Square.Position.X == x) && (squareFront.Square.Position.Y == y) {
			if squareFront.FisherID == 0 {
				return true
			}
		}
	}

	return false
}

func IsUserInOtherMap(idFisher uint64, idMap uint64) bool {
	maps := GetMaps()

	for _, aMap := range maps {
		if aMap.ID != idMap {
			squaresFront := getSquareFrontByMapId(aMap.ID)
			for _, squareFront := range squaresFront {
				if squareFront.FisherID == idFisher {
					return true
				}
			}
		}
	}

	return false
}

func IsFisherInCurrentMap(idFisher uint64, idMap uint64) SquareFront {
	squaresFront := getSquareFrontByMapId(idMap)

	for _, squareFont := range squaresFront {
		if squareFont.FisherID == idFisher {
			return squareFont
		}
	}

	return SquareFront{}
}

func IsUserInThisMap(userId uint64, mapId uint64) bool {
	squaresFront := getSquareFrontByMapId(mapId)
	for _, squareFront := range squaresFront {
		if IsUserGotThisFisher(userId, squareFront.FisherID) {
			return true
		}
	}
	return false
}
