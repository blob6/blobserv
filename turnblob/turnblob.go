package gameblob

import (
	"time"

	datablob "blobfish.com/datablob"
	ws "blobfish.com/servblob/ws"
)

type Turn struct {
	Hub *ws.Hub
}

func (turn *Turn) Run() {
	for {
		time.Sleep(1 * time.Second)

		turn.Hub.BroadcastMap <- datablob.GetMapsAllDatas()
	}

}
