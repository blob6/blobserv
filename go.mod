module serv

go 1.17

require github.com/gorilla/websocket v1.4.2

require blobfish.com/servblob/api v0.0.0
require blobfish.com/servblob/ws v0.0.0
require blobfish.com/datablob v0.0.0
require blobfish.com/helpers v0.0.0
require blobfish.com/gameblob v0.0.0
require blobfish.com/turnblob v0.0.0

require (
	github.com/go-sql-driver/mysql v1.6.0 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.4 // indirect
	golang.org/x/crypto v0.0.0-20211215153901-e495a2d5b3d3 // indirect
	gorm.io/driver/mysql v1.2.2 // indirect
	gorm.io/gorm v1.22.4 // indirect
)

replace blobfish.com/servblob/api v0.0.0 => ./servblob/api
replace blobfish.com/servblob/ws v0.0.0 => ./servblob/ws
replace blobfish.com/datablob v0.0.0 => ./datablob
replace blobfish.com/helpers v0.0.0 => ./helpers
replace blobfish.com/gameblob v0.0.0 => ./gameblob
replace blobfish.com/turnblob v0.0.0 => ./turnblob